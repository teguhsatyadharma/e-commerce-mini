
class Basket():
    '''
    Kelas untuk fitur keranjang belanja
    '''

    def __init__(self, request):
        self.session = request.session
        basket = self.session.get('skey')  # ambil session key
        if 'skey' not in request.session:
            # jika tidak ada session key, maka
            # buat session basket baru
            basket = self.session['skey'] = {}
        # simpan session ke dalam atribut self.basket
        self.basket = basket

    def add(self, product, qty):
        '''
        menambah item ke dalam basket
        '''
        product_id = str(product.id)
        if product_id in self.basket:
            self.basket[product_id]['qty'] = qty
        else:
            self.basket[product_id] = {
                'price': str(product.price), 'qty': int(qty)}
        self.save()

    def __len__(self):
        '''
        Metode untuk menghitung jumlah item dalam keranjang 
        '''
        return sum(item['qty'] for item in self.basket.values())

    def save(self):
        self.session.modified = True
